#!/bin/bash

rm -rf build
rm -rf dest-client
mkdir build
mkdir dest-client

javac -cp ./src -d ./build src/ija/bluerock/labyrinth/board/*.java src/ija/bluerock/labyrinth/Main.java

ulimit -v unlimited
jar cfe ./dest-client/ija2015-client.jar ija.bluerock.labyrinth.Main -C build .

java -ea -jar ./dest-client/ija2015-client.jar
